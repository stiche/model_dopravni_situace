# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | cca 6h |
| jak se mi to podařilo rozplánovat | nic moc |
| návrh designu | [odkaz](./navrh_02/navrh_02.png) |
| proč jsem zvolil tento design | zdál se mi nejlépe vysvětlující |
| zapojení | [odkaz](./navrh_02/tinkercad.png) |
| z jakých součástí se zapojení skládá | z |
| realizace | [odkaz](./fotky) |
| jaký materiál jsem použil a proč | dostupný a efektivní (cín, součástky ze sady, tekuté lepidlo, špejle, novinový papír) |
| co se mi povedlo | Materiálově úsporné řešení|
| co se mi nepovedlo/příště bych udělal/a jinak | Vybral bych si téma jež by mě zajímalo o ždibec více |
| zhodnocení celé tvorby | Přibližně chavlitebné |
