![První návrh](/dokumentace/navrh_01.png)
##
# Model dopravní situace | Moderní jednosměrka

Jednosměrná silnice + Přechod + Okolí

## Co obashuje?

- Led × 18 (Semafory, Vodící světla)
- Tlačítko -> Manuální přepnutí módu DEN/NOC
- Fotorezistory × 2 -> Automatické přepnutí módu DEN/NOC
- 7 Segmentový jednočíslicový monitor (Odpočítávání intervalů)
- vodiče

## Jak to bude fungovat?

### DEN

- Semafory se mění v přibližně 10s intervalu
- Monitor to odpočítává

### NOC

- zahrnuje vše co mód DEN
- přepíná Vodící světla, tak aby odpovídaly provozu