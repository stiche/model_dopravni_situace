// piny

int button = 2;

int RED = 3;
int GREEN = 4;

int RED2 = 5;
int YELLOW2 = 6;
int GREEN2 = 7;

int WHITE1 = 8;
int WHITE2 = 9;
int WHITE3 = 10;


// pomocné proměnné

int STAV = 0;

int pocitadlo = -1;

void setup()
{
  	Serial.begin(9600);
  
  
	// 0)
	pinMode(button,INPUT);
  
  	// 1)
  	pinMode(RED, OUTPUT);
  	pinMode(GREEN, OUTPUT);
  
  	digitalWrite(RED,LOW);
  	digitalWrite(GREEN,HIGH);
  
  	// 2)
  	pinMode(RED2,OUTPUT);
  	pinMode(YELLOW2,OUTPUT);
  	pinMode(GREEN2,OUTPUT);
  
  	digitalWrite(RED2,LOW);
  	digitalWrite(YELLOW2,LOW);
  	digitalWrite(GREEN2,HIGH);
  
  	// 3)
  	pinMode(A0,INPUT);
  	pinMode(A1,INPUT);
  
  	// 4)
  	pinMode(WHITE1, OUTPUT);
  	pinMode(WHITE2, OUTPUT);
  	pinMode(WHITE3, OUTPUT);
  
 	digitalWrite(WHITE1, HIGH);
   	digitalWrite(WHITE2, HIGH);
   	digitalWrite(WHITE3, HIGH);
}

void loop()
{
  	// čtení z tlačítka
  
 	int input = digitalRead(button);
    
  	// čtení z fotorezistoru
  	
  	int input2 = analogRead(A0);
  	int input3 = analogRead(A1);
    
  if(input2 > 200 || input3 > 200) {
  	STAV = 0;
  } else {
  	STAV = 1;
  }
  
  	// stavy
  
  if(STAV == 0) {
  	  // DEN
    
    den();
    
    
  } else if(STAV == 1){
   	  // NOC
    
    noc();
  }
  
  	// doba začátku nového cyklu
  
  	delay(250);
}

void den() {
  pocitadlo++;
  
  // 4)
  digitalWrite(WHITE1, LOW);
  digitalWrite(WHITE2, LOW);
  digitalWrite(WHITE3, LOW);
  
  if(pocitadlo == 0) {
    // 1)
  	digitalWrite(RED,HIGH);
    digitalWrite(GREEN,LOW);
  }
  
  if(pocitadlo == 4) {
  	// 2)
    digitalWrite(RED2,LOW);
    digitalWrite(YELLOW2,LOW);
    digitalWrite(GREEN2,HIGH);
    
    // STOP
  }
  
  if(pocitadlo == 44) {
  	// 2)
    digitalWrite(GREEN2,LOW);
    digitalWrite(YELLOW2,HIGH);
  }
  
  if(pocitadlo == 56) {
  	// 2)
    digitalWrite(YELLOW2,LOW);
    digitalWrite(RED2,HIGH);
    
    // START
  }
  
  if(pocitadlo == 60) {
    // 1)
  	digitalWrite(RED,LOW);
    digitalWrite(GREEN,HIGH);
  }
  
  if(pocitadlo == 112) {
  	digitalWrite(YELLOW2,HIGH);
  }
  
  if(pocitadlo == 120) {
    pocitadlo = -1;
  }
}

void noc() {
  den();
  
  if(pocitadlo > 0 && pocitadlo < 56) {
  	digitalWrite(WHITE1, HIGH);
    digitalWrite(WHITE2, LOW);
    digitalWrite(WHITE3, HIGH);
  }
  
  if(pocitadlo > 56 && pocitadlo < 120) {
  	digitalWrite(WHITE1, HIGH);
    digitalWrite(WHITE2, HIGH);
    digitalWrite(WHITE3, LOW);	
  }
}